from concurrent import futures
import contextlib
import datetime
import logging
import multiprocessing
import socket
import sys
import time

import grpc

import tiler_pb2
import tiler_pb2_grpc

_LOGGER = logging.getLogger(__name__)

_ONE_DAY = datetime.timedelta(days=1)
# _PROCESS_COUNT = multiprocessing.cpu_count()
_PROCESS_COUNT = 4
_THREAD_CONCURRENCY = _PROCESS_COUNT

import taxation_service_pb2_grpc
import taxation_service_pb2

# Example service
class TilerService(tiler_pb2_grpc.TilerServiceServicer):
    def CreateTiles(self, request, context):
        _LOGGER.info("Recieve request: %s", request)

        # Something ....

        with grpc.insecure_channel("localhost:50051") as channel:
            stub = taxation_service_pb2_grpc.TaxationServiceStub(channel)
            response: taxation_service_pb2.ResultTaxationResponse = stub.GetTaxationResult(taxation_service_pb2.ResultTaxationRequest(woodType=[]))

        return tiler_pb2.CreateTilesResponse(path='Path')

# This part are common
def _wait_forever(server):
    try:
        while True:
            time.sleep(_ONE_DAY.total_seconds())
    except KeyboardInterrupt:
        server.stop(None)


def _run_server(bind_address):
    """Start a server in a subprocess."""
    _LOGGER.info("Starting new server.")
    options = (("grpc.so_reuseport", 1),)

    server = grpc.server(
        futures.ThreadPoolExecutor(
            max_workers=_THREAD_CONCURRENCY,
        ),
        options=options,
    )
    tiler_pb2_grpc.add_TilerServiceServicer_to_server(TilerService(), server)
    server.add_insecure_port(bind_address)
    server.start()
    _wait_forever(server)


@contextlib.contextmanager
def _reserve_port():
    """Find and reserve a port for all subprocesses to use."""
    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    if sock.getsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT) == 0:
        raise RuntimeError("Failed to set SO_REUSEPORT.")
    sock.bind(("", 50051))
    try:
        yield sock.getsockname()[1]
    finally:
        sock.close()


def main():
    with _reserve_port() as port:
        bind_address = "localhost:{}".format(port)
        _LOGGER.info("Binding to '%s'", bind_address)
        sys.stdout.flush()
        workers = []
        for _ in range(_PROCESS_COUNT):
            # NOTE: It is imperative that the worker subprocesses be forked before
            # any gRPC servers start up. See
            # https://github.com/grpc/grpc/issues/16001 for more details.
            worker = multiprocessing.Process(
                target=_run_server, args=(bind_address,)
            )
            worker.start()
            workers.append(worker)
        for worker in workers:
            worker.join()


if __name__ == "__main__":
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("[PID %(process)d] %(message)s")
    handler.setFormatter(formatter)
    _LOGGER.addHandler(handler)
    _LOGGER.setLevel(logging.INFO)
    main()
