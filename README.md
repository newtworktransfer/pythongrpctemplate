Tiler
-----

#### Service for generating tiles from GeoToff files.


Build
-----

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Generate proto files
python -m grpc_tools.protoc --python_out=. --grpc_python_out=. --pyi_out=. -I . tiler.proto
python -m grpc_tools.protoc --python_out=. --grpc_python_out=. --pyi_out=. -I . taxation_service.proto
```

Run taxation service
----

```bash
python -m test_taxation_service.taxation_service
```

Run taxation client
----

```bash
python taxation_client.py
```

Run my service
----
```bash
python main.py
```
