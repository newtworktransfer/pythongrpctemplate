import logging

import grpc
import taxation_service_pb2
import taxation_service_pb2_grpc


def run():
    print("Will try to greet world ...")
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = taxation_service_pb2_grpc.TaxationServiceStub(channel)
        response: taxation_service_pb2.ResultTaxationResponse = stub.GetTaxationResult(taxation_service_pb2.ResultTaxationRequest(woodType=[]))
    print("Greeter client received: ", response)


if __name__ == "__main__":
    logging.basicConfig()
    run()
